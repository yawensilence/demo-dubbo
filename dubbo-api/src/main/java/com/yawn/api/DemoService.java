package com.yawn.api;

/**
 * @author yawn
 * @date 2018-05-11 14:18
 */
public interface DemoService {

    String getDemo();

    String hello(String name);

    Person getPerson(String name);
}
