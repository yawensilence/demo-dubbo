package com.yawn.consumer;

import com.yawn.api.DemoService;
import com.yawn.api.Person;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;

/**
 * @author yawn
 * @date 2018-05-11 14:20
 */
public class Consumer {
    public static void main(String[] args) throws IOException {
        ClassPathXmlApplicationContext context =
                new ClassPathXmlApplicationContext("service-consumer.xml");
        context.start();

        DemoService demoService = (DemoService) context.getBean("demoService");

        String demo = demoService.getDemo();
        System.out.println(demo);
        String hello = demoService.hello("world");
        System.out.println(hello);
        Person yawn = demoService.getPerson("yawn");
        System.out.println(yawn);

        System.in.read();
    }
}
