package com.yawn.provider;

import com.yawn.api.DemoService;
import com.yawn.api.Person;

import java.util.Date;

/**
 * @author yawn
 * @date 2018-05-11 14:20
 */
public class Provider implements DemoService {

    public String getDemo() {
        return "demo";
    }

    public String hello(String name) {
        return "Hello " + name + "!";
    }

    public Person getPerson(String name) {
        return new Person(name, 18, new Date());
    }
}
