package com.yawn.provider;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;

/**
 * @author yawn
 * @date 2018-05-11 14:30
 */
public class Deploy {

    public static void main(String[] args) throws IOException {
        System.setProperty("java.net.preferIPv4Stack", "true");
        ClassPathXmlApplicationContext context =
                new ClassPathXmlApplicationContext("service-provider.xml");
        context.start();
        System.out.println("started!");
        // press any key to exit
        System.in.read();
    }
}
